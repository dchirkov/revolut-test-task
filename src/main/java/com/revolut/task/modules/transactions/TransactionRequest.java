package com.revolut.task.modules.transactions;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;

import java.util.UUID;

@Getter
@Builder
public class TransactionRequest {

    private final UUID from;
    private final UUID to;
    private final double amount;

    @JsonCreator
    public TransactionRequest(@JsonProperty("from") UUID from,
                              @JsonProperty("to") UUID to,
                              @JsonProperty("amount") double amount) {
        this.from = from;
        this.to = to;
        this.amount = amount;
    }
}
