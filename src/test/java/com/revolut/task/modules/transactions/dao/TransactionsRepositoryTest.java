package com.revolut.task.modules.transactions.dao;

import com.revolut.task.modules.transactions.Transaction;
import com.revolut.task.modules.transactions.TransactionStatus;
import com.revolut.task.modules.transactions.TransactionsSearchRequest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class TransactionsRepositoryTest {

    private TransactionsRepository repository;

    @BeforeEach
    void setUp() {
        repository = new TransactionsRepositoryImpl();
    }

    @Test
    void testSave() {
        UUID id = UUID.randomUUID();
        repository.save(Transaction.builder()
                                   .id(id)
                                   .build());

        assertNotNull(repository.findOne(id));
    }

    @Test
    void testInvalidSave() {
        Assertions.assertThrows(NullPointerException.class,
                () -> repository.save(Transaction.builder()
                                                 .build()));
    }

    @Test
    void testInvalidUpdateStatus() {
        Assertions.assertThrows(NullPointerException.class,
                () -> repository.updateStatus(null, TransactionStatus.COMPLETED));
    }

    @Test
    void updateStatus() {
        UUID id = UUID.randomUUID();
        repository.save(Transaction.builder()
                                   .id(id)
                                   .status(TransactionStatus.OPENED)
                                   .build());

        repository.updateStatus(id, TransactionStatus.COMPLETED);

        assertNotEquals(TransactionStatus.COMPLETED, repository.findOne(id));
    }

    @Test
    void search() {
        UUID accountId = UUID.randomUUID();

        repository.save(Transaction.builder()
                                   .id(UUID.randomUUID())
                                   .to(accountId)
                                   .build());

        repository.save(Transaction.builder()
                                   .id(UUID.randomUUID())
                                   .to(UUID.randomUUID())
                                   .build());

        repository.save(Transaction.builder()
                                   .id(UUID.randomUUID())
                                   .from(accountId)
                                   .build());

        repository.save(Transaction.builder()
                                   .id(UUID.randomUUID())
                                   .from(UUID.randomUUID())
                                   .build());

        Assertions.assertEquals(2, repository.search(
                TransactionsSearchRequest.builder()
                                         .accounts(Collections.singleton(accountId))
                                         .build()).size());
    }
}
