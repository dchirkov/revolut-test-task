package com.revolut.task.modules.acounts.dao;

import com.revolut.task.modules.acounts.Account;
import com.revolut.task.modules.acounts.AccountsSearchRequest;

import java.util.List;
import java.util.UUID;

public interface AccountsRepository {

    void save(Account account);

    void transfer(UUID accountId, double value);

    void transfer(UUID fromAccountId, UUID toAccountId, double value);

    Account findOne(UUID accountId);

    List<Account> search(AccountsSearchRequest request);

}
