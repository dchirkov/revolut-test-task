package com.revolut.task.modules.transactions.dao;

import com.revolut.task.modules.transactions.Transaction;
import com.revolut.task.modules.transactions.TransactionStatus;
import com.revolut.task.modules.transactions.TransactionsSearchRequest;

import java.util.List;
import java.util.UUID;

public interface TransactionsRepository {

    Transaction save(Transaction transaction);

    Transaction findOne(UUID transactionId);

    void updateStatus(UUID transactionId, TransactionStatus status);

    List<Transaction> search(TransactionsSearchRequest searchRequest);

}
