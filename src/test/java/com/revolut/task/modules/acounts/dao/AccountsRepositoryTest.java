package com.revolut.task.modules.acounts.dao;

import com.revolut.task.modules.acounts.Account;
import com.revolut.task.modules.acounts.AccountsSearchRequest;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
public class AccountsRepositoryTest {

    private AccountsRepository accountsRepository;

    @BeforeEach
    void setUp() {
        accountsRepository = new AccountsRepositoryImpl();
    }

    @Test
    void testSave() {
        UUID accountId = UUID.randomUUID();
        accountsRepository.save(new Account(accountId, 0.0));

        Assertions.assertNotNull(accountsRepository.findOne(accountId));
    }

    @Test
    void testTransferFrom() {
        UUID accountId = UUID.randomUUID();
        accountsRepository.save(new Account(accountId, 500_000.0));

        accountsRepository.transfer(accountId, -200_000.0);

        Assertions.assertEquals(300_000.0, accountsRepository.findOne(accountId).getRemainedBalance());
    }

    @Test
    void testInvalidTransferFrom() {
        UUID accountId = UUID.randomUUID();
        accountsRepository.save(new Account(accountId, 500_000.0));

        Assertions.assertThrows(IllegalArgumentException.class, () -> accountsRepository.transfer(accountId, -600_000.0));
    }

    @Test
    void testTransferTo() {
        UUID accountId = UUID.randomUUID();
        accountsRepository.save(new Account(accountId, 500_000.0));

        accountsRepository.transfer(accountId, 200_000.0);

        Assertions.assertEquals(700_000.0, accountsRepository.findOne(accountId).getRemainedBalance());
    }

    @Test
    void testTransferBetweenAccounts() {
        UUID toAccountId = UUID.randomUUID();
        accountsRepository.save(new Account(toAccountId, 500_000.0));

        UUID fromAccountId = UUID.randomUUID();
        accountsRepository.save(new Account(fromAccountId, 500_000.0));

        accountsRepository.transfer(fromAccountId, toAccountId, 250_000.0);

        Assertions.assertEquals(250_000.0, accountsRepository.findOne(fromAccountId).getRemainedBalance());
        Assertions.assertEquals(750_000.0, accountsRepository.findOne(toAccountId).getRemainedBalance());
    }

    @RepeatedTest(100)
    void testParallelTransferring() throws ExecutionException, InterruptedException {
        UUID accountA = UUID.randomUUID();
        accountsRepository.save(new Account(accountA, 500_000.0));

        UUID accountB = UUID.randomUUID();
        accountsRepository.save(new Account(accountB, 500_000.0));

        CountDownLatch latch = new CountDownLatch(5);

        List<Runnable> tasks = new ArrayList<>();
        tasks.add(() -> transfer(accountB, accountA, 50_000.0, 1_000_000.0, latch));
        tasks.add(() -> transfer(accountA, accountB, 100_000.0, 1_000_000.0, latch));
        tasks.add(() -> transfer(accountB, accountA, 150_000.0, 1_000_000.0, latch));
        tasks.add(() -> transfer(accountA, accountB, 200_000.0, 1_000_000.0, latch));
        tasks.add(() -> transfer(accountB, accountA, 250_000.0, 1_000_000.0, latch));
        Collections.shuffle(tasks);

        ExecutorService executorService = Executors.newFixedThreadPool(3);
        tasks.forEach(executorService::submit);

        latch.await();

        Assertions.assertEquals(650_000.0, accountsRepository.findOne(accountA).getRemainedBalance());
        Assertions.assertEquals(350_000.0, accountsRepository.findOne(accountB).getRemainedBalance());
    }

    private void transfer(UUID fromAccountId, UUID toAccountId, double value, double balance, CountDownLatch latch) {
        try {
            log.debug("Check balance before transaction");
            checkBalance(toAccountId, fromAccountId, balance);

            log.debug("Transfer {} -> {} -> {}", fromAccountId, value, toAccountId);
            accountsRepository.transfer(fromAccountId, toAccountId, value);
            log.debug("Transferred {} -> {} -> {}", fromAccountId, value, toAccountId);

            log.debug("Check balance after transaction");
            checkBalance(toAccountId, fromAccountId, balance);
        } catch (Throwable e) {
            log.error("Error during transferring", e);
        } finally {
            latch.countDown();
        }
    }

    private void checkBalance(UUID toAccountId, UUID fromAccountId, double sum) {
        List<Account> accounts = accountsRepository.search(
                AccountsSearchRequest.builder()
                                     .accounts(Stream.of(toAccountId, fromAccountId).collect(Collectors.toSet()))
                                     .build());
        double actual = accounts.stream().mapToDouble(Account::getRemainedBalance).sum();
        Assertions.assertEquals(sum, actual, () -> "Common remaining balance " + actual + " is not equal to expected " + sum);
    }
}
