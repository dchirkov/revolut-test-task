package com.revolut.task.modules.transactions.api;

import com.revolut.task.modules.transactions.Transaction;
import com.revolut.task.modules.transactions.TransactionRequest;
import com.revolut.task.modules.transactions.TransactionStatus;
import com.revolut.task.modules.transactions.TransactionsSearchRequest;

import java.util.List;
import java.util.UUID;

public interface TransactionsService {

    Transaction create(TransactionRequest request);

    Transaction getOne(UUID transactionId);

    void updateStatus(UUID transactionId, TransactionStatus status);

    List<Transaction> search(TransactionsSearchRequest searchRequest);

}
