package com.revolut.task.modules.transactions;

import lombok.Builder;
import lombok.Getter;

import java.util.Set;
import java.util.UUID;

@Getter
@Builder
public class TransactionsSearchRequest {

    private final Set<UUID> accounts;
}
