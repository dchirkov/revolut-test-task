package com.revolut.task.modules.transactions.api;

import com.revolut.task.modules.transactions.Transaction;
import com.revolut.task.modules.transactions.TransactionRequest;
import com.revolut.task.modules.transactions.TransactionStatus;
import com.revolut.task.modules.transactions.TransactionsSearchRequest;
import com.revolut.task.modules.transactions.dao.TransactionsRepository;
import lombok.Setter;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class TransactionsServiceImpl implements TransactionsService {

    @Setter
    private TransactionsRepository repository;

    @Override
    public Transaction create(TransactionRequest request) {
        return repository.save(Transaction.builder()
                                          .id(UUID.randomUUID())
                                          .from(request.getFrom())
                                          .to(request.getTo())
                                          .amount(request.getAmount())
                                          .status(TransactionStatus.OPENED)
                                          .created(LocalDateTime.now(ZoneId.of("UTC")))
                                          .build());
    }

    @Override
    public Transaction getOne(UUID transactionId) {
        if (Objects.isNull(transactionId)) {
            return null;
        }
        return repository.findOne(transactionId);
    }

    @Override
    public void updateStatus(UUID transactionId, TransactionStatus status) {
        repository.updateStatus(transactionId, status);
    }

    @Override
    public List<Transaction> search(TransactionsSearchRequest searchRequest) {
        return repository.search(searchRequest);
    }
}
