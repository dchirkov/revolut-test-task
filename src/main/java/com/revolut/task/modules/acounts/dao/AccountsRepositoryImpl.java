package com.revolut.task.modules.acounts.dao;

import com.revolut.task.modules.acounts.Account;
import com.revolut.task.modules.acounts.AccountsSearchRequest;
import com.revolut.task.modules.utils.GlobalLock;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

@Slf4j
public class AccountsRepositoryImpl implements AccountsRepository {

    private final Map<UUID, AccountRecord> accounts = new ConcurrentHashMap<>();
    private final GlobalLock globalLock = new GlobalLock();

    @Override
    public void save(Account account) {
        globalLock.doWithLocalSync(() -> {
            AccountRecord record = new AccountRecord();
            record.setId(account.getAccountId());
            record.setBalance(account.getRemainedBalance());
            record.setLock(new ReentrantLock());

            accounts.put(account.getAccountId(), record);
        });
    }

    @Override
    public void transfer(UUID accountId, double value) {
        globalLock.doWithLocalSync(() -> {
            AccountRecord record = accounts.get(accountId);

            Lock lock = record.getLock();
            lock.lock();
            try {
                double balance = record.getBalance();
                double remainedBalance = balance + value;
                if (0.0 > remainedBalance) {
                    throw new IllegalArgumentException("Remained balance is negative for account " + accountId);
                }
                record.setBalance(remainedBalance);
            } finally {
                lock.unlock();
            }
        });
    }

    @Override
    public void transfer(UUID fromAccountId, UUID toAccountId, double value) {
        globalLock.doWithGlobalLock(() -> {
            AccountRecord fromRecord = accounts.get(fromAccountId);
            AccountRecord toRecord = accounts.get(toAccountId);

            double balance = fromRecord.getBalance();
            double remainedBalance = balance - value;
            if (0.0 > remainedBalance) {
                throw new IllegalArgumentException("Remained balance is negative for account " + fromRecord.getId());
            }
            fromRecord.setBalance(remainedBalance);

            balance = toRecord.getBalance();
            remainedBalance = balance + value;
            toRecord.setBalance(remainedBalance);
        });
    }

    @Override
    public Account findOne(UUID accountId) {
        return globalLock.callWithLocalSync(() -> {
            AccountRecord record = accounts.get(accountId);
            if (Objects.isNull(record)) {
                return null;
            }

            Lock lock = record.getLock();
            lock.lock();
            try {
                return new Account(record.getId(), record.getBalance());
            } finally {
                lock.unlock();
            }
        });
    }

    @Override
    public List<Account> search(AccountsSearchRequest request) {
        return globalLock.callWithGlobalLock(() -> Optional.ofNullable(request.getAccounts())
                                                           .orElse(accounts.keySet())
                                                           .stream()
                                                           .map(accounts::get)
                                                           .filter(Objects::nonNull)
                                                           .map(r -> new Account(r.getId(), r.getBalance()))
                                                           .collect(Collectors.toList()));
    }

    @NoArgsConstructor
    @Getter
    @Setter
    private static class AccountRecord {

        private UUID id;
        private volatile double balance;
        private Lock lock;
    }
}
