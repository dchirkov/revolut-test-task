package com.revolut.task.modules.exchange;

import lombok.AllArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.Objects;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import java.util.stream.IntStream;

@Slf4j
public class ExchangeService {

    private static final int THREADS_COUNT = 5;

    @Setter
    private TransferCommandExecutor transferCommandExecutor;
    private final Queue<TransferCommand> commandQueue;
    private final ExecutorService executorService;
    private final Semaphore semaphore = new Semaphore(1, true);

    public ExchangeService() {
        commandQueue = new ConcurrentLinkedQueue<>();
        executorService = Executors.newFixedThreadPool(THREADS_COUNT);

        IntStream.range(0, THREADS_COUNT)
                 .mapToObj(i -> new TransferJob())
                 .forEach(executorService::submit);
    }

    public void add(TransferCommand command) {
        commandQueue.add(command);
        semaphore.release();
    }

    public void destroy() {
        executorService.shutdown();
    }

    @AllArgsConstructor
    private class TransferJob implements Runnable {

        @Override
        public void run() {
            log.debug("Run polling transfer job");
            while (!Thread.currentThread().isInterrupted()) {
                TransferCommand command;
                while (Objects.nonNull(command = commandQueue.poll())) {
                    transferCommandExecutor.execute(command);
                }
                log.debug("Wait for new command");
                try {
                    semaphore.acquire();
                } catch (InterruptedException e) {
                    log.error(e.getMessage(), e);
                }
            }
            log.debug("Stop polling transfer job");
        }
    }
}
