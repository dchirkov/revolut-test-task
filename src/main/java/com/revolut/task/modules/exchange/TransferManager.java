package com.revolut.task.modules.exchange;

import com.revolut.task.modules.transactions.Transaction;
import com.revolut.task.modules.transactions.TransactionRequest;

public interface TransferManager {

    /**
     * Create money transaction according to transaction request.
     *
     * @param request - contains information about accounts and money amount.
     * @return {@code Transaction} instance
     */
    Transaction transfer(TransactionRequest request);

}
