package com.revolut.task.modules.exchange;

import com.revolut.task.modules.acounts.Account;
import com.revolut.task.modules.acounts.api.AccountsService;
import com.revolut.task.modules.transactions.Transaction;
import com.revolut.task.modules.transactions.TransactionRequest;
import com.revolut.task.modules.transactions.api.TransactionsService;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.Objects;

@Slf4j
@Setter
public class TransferManagerImpl implements TransferManager {

    private ExchangeService exchangeService;
    private AccountsService accountsService;
    private TransactionsService transactionsService;

    @Override
    public Transaction transfer(TransactionRequest request) {
        log.info("Transfer money ({}) from account {} to account {}",
                request.getAmount(), request.getFrom(), request.getTo());

        validateTransactionRequest(request);

        // Create money transaction
        Transaction transaction = transactionsService.create(request);

        // Execute money transaction
        exchangeService.add(TransferCommand.builder()
                                           .transactionId(transaction.getId())
                                           .fromAccountId(request.getFrom())
                                           .toAccountId(request.getTo())
                                           .build());
        return transaction;
    }

    private void validateTransactionRequest(TransactionRequest request) {
        if (0.0 >= request.getAmount()) {
            throw new IllegalArgumentException("Amount should be more then 0.0");
        }

        if (Objects.nonNull(request.getFrom())) {
            Account fromAccount = accountsService.get(request.getFrom());
            if (Objects.nonNull(fromAccount)) {
                // check remaining balance
                if (fromAccount.getRemainedBalance() < request.getAmount()) {
                    throw new IllegalArgumentException("Cannot create transaction, the remaining balance is not enough");
                }
            } else {
                throw new IllegalArgumentException("Cannot find account " + request.getFrom());
            }
        }

        if (Objects.nonNull(request.getTo())) {
            Account toAccount = accountsService.get(request.getTo());
            if (Objects.isNull(toAccount)) {
                throw new IllegalArgumentException("Cannot find account " + request.getTo());
            }
        }

        if (Objects.isNull(request.getTo()) && Objects.isNull(request.getFrom())) {
            throw new IllegalArgumentException("At least one account should be defined");
        }
        if (Objects.equals(request.getFrom(), request.getTo())) {
            throw new IllegalArgumentException("Accounts should be different");
        }
    }
}
