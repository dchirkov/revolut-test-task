package com.revolut.task.context;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.revolut.task.modules.acounts.api.AccountsService;
import com.revolut.task.modules.acounts.api.AccountsServiceImpl;
import com.revolut.task.modules.acounts.dao.AccountsRepository;
import com.revolut.task.modules.acounts.dao.AccountsRepositoryImpl;
import com.revolut.task.modules.exchange.*;
import com.revolut.task.modules.transactions.api.TransactionsService;
import com.revolut.task.modules.transactions.api.TransactionsServiceImpl;
import com.revolut.task.modules.transactions.dao.TransactionsRepository;
import com.revolut.task.modules.transactions.dao.TransactionsRepositoryImpl;
import lombok.Getter;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ApplicationContext {

    private final Map<Class, Object> beans = new ConcurrentHashMap<>();

    @Getter
    private final ObjectMapper mapper;

    private ApplicationContext() {
        AccountsRepository accountsRepository = new AccountsRepositoryImpl();
        addBean(AccountsRepository.class, accountsRepository);

        TransactionsRepository transactionsRepository = new TransactionsRepositoryImpl();
        addBean(TransactionsRepository.class, transactionsRepository);

        AccountsServiceImpl accountsService = new AccountsServiceImpl();
        accountsService.setRepository(get(AccountsRepository.class));
        addBean(AccountsService.class, accountsService);

        TransactionsServiceImpl transactionService = new TransactionsServiceImpl();
        transactionService.setRepository(get(TransactionsRepository.class));
        addBean(TransactionsService.class, transactionService);

        TransferCommandExecutorImpl transferExecutor = new TransferCommandExecutorImpl();
        transferExecutor.setAccountsService(get(AccountsService.class));
        transferExecutor.setTransactionsService(get(TransactionsService.class));
        addBean(TransferCommandExecutor.class, transferExecutor);

        ExchangeService exchangeService = new ExchangeService();
        exchangeService.setTransferCommandExecutor(get(TransferCommandExecutor.class));
        addBean(ExchangeService.class, exchangeService);

        TransferManagerImpl transferManager = new TransferManagerImpl();
        transferManager.setExchangeService(get(ExchangeService.class));
        transferManager.setAccountsService(get(AccountsService.class));
        transferManager.setTransactionsService(get(TransactionsService.class));
        addBean(TransferManager.class, transferManager);

        mapper = new ObjectMapper();
        mapper.registerModule(new JavaTimeModule());
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    }

    private <T> void addBean(Class<T> tClass, T bean) {
        beans.put(tClass, bean);
    }

    public <T> T get(Class<T> tClass) {
        return (T)beans.get(tClass);
    }

    private static class ApplicationContextHolder {

        private static final ApplicationContext CTX = new ApplicationContext();
    }

    public static ApplicationContext getInstance() {
        return ApplicationContextHolder.CTX;
    }
}
