package com.revolut.task;

import com.revolut.task.context.ApplicationContext;
import com.revolut.task.modules.acounts.Account;
import com.revolut.task.modules.acounts.AccountsSearchRequest;
import com.revolut.task.modules.acounts.api.AccountsService;
import com.revolut.task.modules.exchange.TransferManager;
import com.revolut.task.modules.transactions.Transaction;
import com.revolut.task.modules.transactions.TransactionRequest;
import com.revolut.task.modules.transactions.TransactionsSearchRequest;
import com.revolut.task.modules.transactions.api.TransactionsService;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import spark.Spark;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.Collections.singleton;
import static java.util.UUID.fromString;

@Slf4j
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Application {

    private static final String APPLICATION_JSON = "application/json";

    public static void main(String... args) {
        ApplicationContext ctx = ApplicationContext.getInstance();

        Map<String, String> params = new HashMap<>();
        if (0 < args.length) {
            for (int i = 0; i < args.length; i += 2) {
                params.put(args[i], args[i + 1]);
            }
        }

        log.info("Start Spark server: {}", params);
        // init spark server
        Spark.port(Integer.parseInt(params.getOrDefault("-p", "8080")));

        Spark.post("/account", (req, res) -> {
            Account account = ctx.getMapper().readValue(req.body(), Account.class);
            account = ctx.get(AccountsService.class).create(account);
            res.type(APPLICATION_JSON);
            return ctx.getMapper().writeValueAsString(account);
        });

        Spark.get("/accounts/:accountId", (req, res) -> {
            Account account = ctx.get(AccountsService.class).get(fromString(req.params("accountId")));
            res.type(APPLICATION_JSON);
            return ctx.getMapper().writeValueAsString(account);
        });

        Spark.get("/accounts", (req, res) -> {
            List<Account> accounts = ctx.get(AccountsService.class).search(
                    AccountsSearchRequest.builder()
                                         .build());
            res.type(APPLICATION_JSON);
            return ctx.getMapper().writeValueAsString(accounts);
        });

        Spark.post("/transaction", (req, res) -> {
            TransactionRequest request = ctx.getMapper().readValue(req.body(), TransactionRequest.class);
            TransferManager transferManager = ctx.get(TransferManager.class);
            res.type(APPLICATION_JSON);
            return ctx.getMapper().writeValueAsString(transferManager.transfer(request));
        });

        Spark.get("/transactions/:transactionId", (req, res) -> {
            Transaction transaction = ctx.get(TransactionsService.class).getOne(fromString(req.params("transactionId")));
            res.type(APPLICATION_JSON);
            return ctx.getMapper().writeValueAsString(transaction);
        });

        Spark.get("/transactions", (req, res) -> {
            List<Transaction> transactions = ctx.get(TransactionsService.class).search(
                    TransactionsSearchRequest.builder()
                                             .accounts(singleton(fromString(req.queryParams("accountId"))))
                                             .build());
            res.type(APPLICATION_JSON);
            return ctx.getMapper().writeValueAsString(transactions);
        });
    }

}
