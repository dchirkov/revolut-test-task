package com.revolut.task.modules.acounts.api;

import com.revolut.task.modules.acounts.Account;
import com.revolut.task.modules.acounts.AccountsSearchRequest;

import java.util.List;
import java.util.UUID;

public interface AccountsService {

    /**
     * Create empty account with 0 remained balance
     *
     * @param account
     * @return {@code Account} instance
     */
    Account create(Account account);

    /**
     * Find and return account by it's id.
     *
     * @param accountId
     * @return {@code Account} instance
     */
    Account get(UUID accountId);

    /**
     * Search for accounts according to provided request.
     *
     * @param {@code AccountsSearchRequest} request
     * @return collection of accounts
     */
    List<Account> search(AccountsSearchRequest request);

    /**
     * Change account balance according to provided money amount.
     *
     * @param accountId
     * @param value - amount of money, that should be added to account balance if value > 0,
     *              if value < 0 then account balance would be decreased.
     */
    void transfer(UUID accountId, double value);

    /**
     * Transfer money between accounts.
     *
     * @param fromAccountId
     * @param toAccountId
     * @param value - amount of money
     */
    void transfer(UUID fromAccountId, UUID toAccountId, double value);
}
