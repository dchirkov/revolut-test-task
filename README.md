# Revolut Test Task

RESTfull API for money transfers between accounts.

## Main components

* Accounts (api, repository)
* Transactions (api, repository)
* Exchange - contains main API for creating and executing transactions

## Start application server
_Build executable jar_
```
mvn clean compile assembly:single 
``` 
_Start application server_
```
java -jar revolut-test-task-1.0-SNAPSHOT-jar-with-dependencies.jar -p 8080
```

## Common flow
* Create accounts

```
POST http://localhost:8080/account
{
    "accountId": "1168b3b1-1eb6-41b6-839d-4dd35459e586",
    "remainedBalance": 50000.0
}
POST http://localhost:8080/account
{
    "accountId": "a871656c-1775-4347-9ae6-0318d77db805",
    "remainedBalance": 50000.0
}
```
* Create transactions between accounts
```
POST http://localhost:8080/transaction
{
    "to": "a871656c-1775-4347-9ae6-0318d77db805",
    "amount": 50000
}

POST http://localhost:8080/transaction
{
    "to": "1168b3b1-1eb6-41b6-839d-4dd35459e586",
    "amount": 50000
}

POST http://localhost:8080/transaction
{
    "from": "1168b3b1-1eb6-41b6-839d-4dd35459e586",
    "to": "a871656c-1775-4347-9ae6-0318d77db805",
    "amount": 25000
}
```
* Create transfer command and put it into commands queue
* Poll commands queue and execute every command in separate thread
* Execute command and update transaction status according to result
* Check accounts remaining balance
```
GET http://localhost:8080/accounts
[
    {
        "accountId": "1168b3b1-1eb6-41b6-839d-4dd35459e586",
        "remainedBalance": 25000.0
    },
    {
        "accountId": "a871656c-1775-4347-9ae6-0318d77db805",
        "remainedBalance": 25000.0
    }
]
```
* Check account transactions
```
GET http://localhost:8080/transactions?accountId=1168b3b1-1eb6-41b6-839d-4dd35459e586
[
    {
        "id": "9476ca9a-ddfe-4687-9b12-e331a3461ca9",
        "from": null,
        "to": "1168b3b1-1eb6-41b6-839d-4dd35459e586",
        "amount": 50000.0,
        "created": "2020-01-31T16:56:02.43",
        "status": "COMPLETED"
    },
    {
        "id": "8123fbf9-b898-48d9-b095-5a7093fa0979",
        "from": "1168b3b1-1eb6-41b6-839d-4dd35459e586",
        "to": "a871656c-1775-4347-9ae6-0318d77db805",
        "amount": 25000.0,
        "created": "2020-01-31T16:56:41.592",
        "status": "COMPLETED"
    }
]
```
