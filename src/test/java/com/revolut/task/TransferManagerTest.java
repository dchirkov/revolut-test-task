package com.revolut.task;

import com.revolut.task.context.ApplicationContext;
import com.revolut.task.modules.acounts.Account;
import com.revolut.task.modules.acounts.api.AccountsService;
import com.revolut.task.modules.exchange.ExchangeService;
import com.revolut.task.modules.exchange.TransferManager;
import com.revolut.task.modules.transactions.Transaction;
import com.revolut.task.modules.transactions.TransactionRequest;
import com.revolut.task.modules.transactions.TransactionStatus;
import com.revolut.task.modules.transactions.TransactionsSearchRequest;
import com.revolut.task.modules.transactions.api.TransactionsService;
import com.revolut.task.modules.transactions.api.TransactionsServiceImpl;
import com.revolut.task.modules.transactions.dao.TransactionsRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.*;
import org.mockito.Mockito;

import java.util.*;
import java.util.concurrent.Phaser;
import java.util.stream.IntStream;

@Slf4j
public class TransferManagerTest {

    private AccountsService accountsService;
    private TransferManager transferManager;
    private TransactionsService transactionsService;
    private TransactionsRepository transactionsRepository;
    private TransactionsRepository transactionsRepositorySpy;

    private static ApplicationContext ctx;

    @BeforeAll
    public static void beforeAll() {
        ctx = ApplicationContext.getInstance();
    }

    @BeforeEach
    public void setUp() {
        accountsService = ctx.get(AccountsService.class);
        transferManager = ctx.get(TransferManager.class);
        transactionsService = ctx.get(TransactionsService.class);

        transactionsRepository = ctx.get(TransactionsRepository.class);
        transactionsRepositorySpy = Mockito.spy(transactionsRepository);
        ((TransactionsServiceImpl)transactionsService).setRepository(transactionsRepositorySpy);
    }

    @Test
    public void testTransferringToAccount() throws InterruptedException {
        Account account = accountsService.create(new Account());
        waitForTransaction(transferManager.transfer(TransactionRequest.builder()
                                                                      .amount(1_000_000.0)
                                                                      .to(account.getAccountId())
                                                                      .build()));

        Assertions.assertEquals(1_000_000.0, accountsService.get(account.getAccountId()).getRemainedBalance());
    }

    @Test
    void testInvalidTransferringToAccount() {
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> transferManager.transfer(TransactionRequest.builder()
                                                                 .to(UUID.randomUUID())
                                                                 .amount(600_000.0)
                                                                 .build()));
    }

    @Test
    @Timeout(10)
    public void testTransferringFromAccount() throws InterruptedException {
        Account account = accountsService.create(new Account());

        waitForTransaction(transferManager.transfer(TransactionRequest.builder()
                                                                      .amount(1_000_000.0)
                                                                      .to(account.getAccountId())
                                                                      .build()));

        waitForTransaction(transferManager.transfer(TransactionRequest.builder()
                                                                      .from(account.getAccountId())
                                                                      .amount(500_000.0)
                                                                      .build()));

        Assertions.assertEquals(500_000.0, accountsService.get(account.getAccountId()).getRemainedBalance());
    }

    @Test
    @Timeout(10)
    public void testTransferringFromAccountToAnother() throws InterruptedException {
        Account fromAccount = accountsService.create(new Account());
        waitForTransaction(transferManager.transfer(TransactionRequest.builder()
                                                                      .amount(1_000_000.0)
                                                                      .to(fromAccount.getAccountId())
                                                                      .build()));

        Account toAccount = accountsService.create(new Account());
        waitForTransaction(transferManager.transfer(TransactionRequest.builder()
                                                                      .from(fromAccount.getAccountId())
                                                                      .to(toAccount.getAccountId())
                                                                      .amount(500_000.0)
                                                                      .build()));

        Assertions.assertEquals(500_000.0, accountsService.get(fromAccount.getAccountId()).getRemainedBalance());
        Assertions.assertEquals(500_000.0, accountsService.get(toAccount.getAccountId()).getRemainedBalance());
    }

    @Test
    public void testInvalidTransferringFromAccount() throws InterruptedException {
        Account account = accountsService.create(new Account());

        waitForTransaction(transferManager.transfer(TransactionRequest.builder()
                                                                      .amount(500_000.0)
                                                                      .to(account.getAccountId())
                                                                      .build()));

        Assertions.assertThrows(IllegalArgumentException.class,
                () -> transferManager.transfer(TransactionRequest.builder()
                                                                 .from(account.getAccountId())
                                                                 .amount(600_000.0)
                                                                 .build()));

        Assertions.assertThrows(IllegalArgumentException.class,
                () -> transferManager.transfer(TransactionRequest.builder()
                                                                 .from(UUID.randomUUID())
                                                                 .amount(600_000.0)
                                                                 .build()));
    }

    @Test
    @Timeout(10)
    public void testInvalidTransferringFromAccountToAnother() throws InterruptedException {
        Account fromAccount = accountsService.create(new Account());
        waitForTransaction(transferManager.transfer(TransactionRequest.builder()
                                                                      .amount(500_000.0)
                                                                      .to(fromAccount.getAccountId())
                                                                      .build()));

        Account toAccount = accountsService.create(new Account());
        Assertions.assertThrows(IllegalArgumentException.class,
                () -> transferManager.transfer(TransactionRequest.builder()
                                                                 .from(fromAccount.getAccountId())
                                                                 .to(toAccount.getAccountId())
                                                                 .amount(600_000.0)
                                                                 .build()));
    }

    @Test
    void testInvalidTransferringBetweenSameAccounts() {
        Account account = accountsService.create(new Account());

        Assertions.assertThrows(IllegalArgumentException.class,
                () -> transferManager.transfer(TransactionRequest.builder()
                                                                 .from(account.getAccountId())
                                                                 .to(account.getAccountId())
                                                                 .amount(600_000.0)
                                                                 .build()));
    }

    @Timeout(20)
    @RepeatedTest(10)
    public void testRemainingBalanceNotNegative() throws InterruptedException {
        int accountsCount = 10;

        Phaser phaser = new Phaser(1);
        Mockito.doAnswer(a -> {
            // make transaction execution to be slower
            Thread.sleep(100);

            UUID transactionId = a.getArgument(0);
            TransactionStatus status = a.getArgument(1);
            transactionsRepository.updateStatus(transactionId, status);

            log.debug("Phase {}: change transaction {} status {}", phaser.getPhase(), transactionId, status);
            if (status.isTerminal()) {
                phaser.arriveAndDeregister();
            }
            return null;
        }).when(transactionsRepositorySpy).updateStatus(Mockito.any(), Mockito.any());

        Set<UUID> accounts = new HashSet<>();

        IntStream.range(0, accountsCount)
                 .mapToObj(i -> accountsService.create(new Account()))
                 .peek(a -> accounts.add(a.getAccountId()))
                 .forEach(a -> {
                     phaser.register();
                     transferManager.transfer(TransactionRequest.builder()
                                                                .amount(500_000.0)
                                                                .to(a.getAccountId())
                                                                .build());
                 });

        phaser.arriveAndAwaitAdvance();

        accounts.forEach(accountId -> {
            phaser.register();
            // it is possible to withdraw money from account
            transferManager.transfer(TransactionRequest.builder()
                                                       .amount(300_000.0)
                                                       .from(accountId)
                                                       .build());

            phaser.register();
            // it is possible to withdraw money from account again (current remaining balance should be more then 300_000.0)
            transferManager.transfer(TransactionRequest.builder()
                                                       .amount(300_000.0)
                                                       .from(accountId)
                                                       .build());
        });

        phaser.arriveAndAwaitAdvance();

        List<Transaction> transactions = transactionsService.search(TransactionsSearchRequest.builder()
                                                                                             .accounts(accounts)
                                                                                             .build());

        Assertions.assertEquals(accountsCount * 3, transactions.size());
        Assertions.assertTrue(transactions.stream().allMatch(tr -> tr.getStatus().isTerminal()));
        Assertions.assertEquals(accountsCount, transactions.stream().filter(tr -> TransactionStatus.FAILED.equals(tr.getStatus())).count());
    }

    @Test
    void testParallelExecution() throws InterruptedException {
        Account account = accountsService.create(new Account());

        Random random = new Random();
        double sum = random.doubles(1000, 1_000.0, 10_000.0)
                           .map(v -> (long)v)
                           .peek(i -> transferManager.transfer(TransactionRequest.builder()
                                                                                 .amount(i)
                                                                                 .to(account.getAccountId())
                                                                                 .build()))
                           .sum();

        while (accountsService.get(account.getAccountId()).getRemainedBalance() < sum) {
            Thread.sleep(1000);
        }

        Assertions.assertEquals(accountsService.get(account.getAccountId()).getRemainedBalance(), sum);
    }

    @AfterAll
    public static void afterAll() {
        ExchangeService exchangeService = ctx.get(ExchangeService.class);
        exchangeService.destroy();
    }

    private void waitForTransaction(Transaction transaction) throws InterruptedException {
        while (!TransactionStatus.COMPLETED.equals(transactionsService.getOne(transaction.getId()).getStatus())) {
            Thread.sleep(1000);
        }
    }
}
