package com.revolut.task.modules.exchange;

public interface TransferCommandExecutor {

    void execute(TransferCommand command);

}
