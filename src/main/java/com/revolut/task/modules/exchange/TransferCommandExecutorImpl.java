package com.revolut.task.modules.exchange;

import com.revolut.task.modules.acounts.api.AccountsService;
import com.revolut.task.modules.transactions.Transaction;
import com.revolut.task.modules.transactions.TransactionStatus;
import com.revolut.task.modules.transactions.api.TransactionsService;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.Objects;
import java.util.UUID;

@Slf4j
@Setter
public class TransferCommandExecutorImpl implements TransferCommandExecutor {

    private AccountsService accountsService;
    private TransactionsService transactionsService;

    @Override
    public void execute(TransferCommand command) {
        UUID transactionId = command.getTransactionId();
        Transaction transaction = transactionsService.getOne(transactionId);
        try {
            log.debug("Start to execute transaction {}", transactionId);
            transactionsService.updateStatus(transactionId, TransactionStatus.IN_PROGRESS);

            if (Objects.isNull(command.getToAccountId())) {
                accountsService.transfer(command.getFromAccountId(), -1 * transaction.getAmount());
            } else if (Objects.isNull(command.getFromAccountId())) {
                accountsService.transfer(command.getToAccountId(), transaction.getAmount());
            } else {
                accountsService.transfer(command.getFromAccountId(), command.getToAccountId(), transaction.getAmount());
            }

            log.debug("Complete transaction {}", transactionId);
            transactionsService.updateStatus(transactionId, TransactionStatus.COMPLETED);
        } catch (Exception e) {
            log.error(String.format("Cannot complete transaction %s", transactionId), e);
            transactionsService.updateStatus(transactionId, TransactionStatus.FAILED);
        }
    }
}
