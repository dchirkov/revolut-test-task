package com.revolut.task.modules.acounts;

import lombok.Builder;
import lombok.Getter;

import java.util.Set;
import java.util.UUID;

@Builder
@Getter
public class AccountsSearchRequest {

    private final Set<UUID> accounts;

}
