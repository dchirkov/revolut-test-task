package com.revolut.task.modules.transactions;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Builder
public class Transaction {

    private final UUID id;
    private final UUID from;
    private final UUID to;
    private final double amount;
    private final LocalDateTime created;

    @Setter
    private TransactionStatus status;

}
