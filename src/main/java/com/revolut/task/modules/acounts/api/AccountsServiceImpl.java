package com.revolut.task.modules.acounts.api;

import com.revolut.task.modules.acounts.Account;
import com.revolut.task.modules.acounts.AccountsSearchRequest;
import com.revolut.task.modules.acounts.dao.AccountsRepository;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.List;
import java.util.Objects;
import java.util.UUID;

@Slf4j
public class AccountsServiceImpl implements AccountsService {

    @Setter
    private AccountsRepository repository;

    @Override
    public Account create(Account account) {
        if (Objects.isNull(account.getAccountId())) {
            account.setAccountId(UUID.randomUUID());
        }
        repository.save(account);
        return account;
    }

    @Override
    public Account get(UUID accountId) {
        if (Objects.isNull(accountId)) {
            return null;
        }
        return repository.findOne(accountId);
    }

    @Override
    public List<Account> search(AccountsSearchRequest request) {
        return repository.search(request);
    }

    @Override
    public void transfer(UUID accountId, double value) {
        log.debug("Transfer money: [{}] -> {}", accountId, value);
        repository.transfer(accountId, value);
    }

    @Override
    public void transfer(UUID fromAccountId, UUID toAccountId, double value) {
        log.debug("Transfer money: [{}] -> {} -> [{}]", fromAccountId, value, toAccountId);
        repository.transfer(fromAccountId, toAccountId, value);
    }
}
