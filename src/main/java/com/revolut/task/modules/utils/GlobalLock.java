package com.revolut.task.modules.utils;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.function.Supplier;

public class GlobalLock {

    private final ReadWriteLock globalLock = new ReentrantReadWriteLock();

    public <T> T callWithLocalSync(Supplier<T> execution) {
        Lock lock = globalLock.readLock();
        lock.lock();
        try {
            return execution.get();
        } finally {
            lock.unlock();
        }
    }

    public <T> T callWithGlobalLock(Supplier<T> execution) {
        Lock lock = globalLock.writeLock();
        lock.lock();
        try {
            return execution.get();
        } finally {
            lock.unlock();
        }
    }

    public void doWithLocalSync(Runnable execution) {
        Lock lock = globalLock.readLock();
        lock.lock();
        try {
            execution.run();
        } finally {
            lock.unlock();
        }
    }

    public void doWithGlobalLock(Runnable execution) {
        Lock lock = globalLock.writeLock();
        lock.lock();
        try {
            execution.run();
        } finally {
            lock.unlock();
        }
    }

}
