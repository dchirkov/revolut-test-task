package com.revolut.task.modules.transactions.dao;

import com.revolut.task.modules.transactions.Transaction;
import com.revolut.task.modules.transactions.TransactionStatus;
import com.revolut.task.modules.transactions.TransactionsSearchRequest;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import static java.util.Objects.requireNonNull;

public class TransactionsRepositoryImpl implements TransactionsRepository {

    private final Map<UUID, Transaction> transactions = new ConcurrentHashMap<>();

    @Override
    public Transaction save(Transaction transaction) {
        transactions.put(requireNonNull(transaction.getId(), "Transaction id is required"), transaction);
        return transaction;
    }

    @Override
    public Transaction findOne(UUID transactionId) {
        return transactions.get(requireNonNull(transactionId, "Transaction id is required"));
    }

    @Override
    public void updateStatus(UUID transactionId, TransactionStatus status) {
        Optional.ofNullable(transactions.get(requireNonNull(transactionId, "Transaction id is required")))
                .ifPresent(transaction -> transaction.setStatus(status));
    }

    @Override
    public List<Transaction> search(TransactionsSearchRequest searchRequest) {
        Set<UUID> accounts = searchRequest.getAccounts();
        if (Objects.isNull(accounts) || accounts.isEmpty()) {
            return Collections.emptyList();
        }
        return transactions.values().stream()
                           .filter(tr -> Objects.nonNull(tr.getTo()) && accounts.contains(tr.getTo())
                                   || Objects.nonNull(tr.getFrom()) && accounts.contains(tr.getFrom()))
                           .collect(Collectors.toList());
    }
}
