package com.revolut.task.modules.transactions;

import lombok.Getter;

public enum TransactionStatus {

    OPENED(false),
    IN_PROGRESS(false),
    FAILED(true),
    COMPLETED(true);

    @Getter
    private final boolean terminal;

    TransactionStatus(boolean terminal) {
        this.terminal = terminal;
    }
}
