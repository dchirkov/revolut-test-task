package com.revolut.task.modules.exchange;

import lombok.Builder;
import lombok.Getter;

import java.util.UUID;

@Builder
@Getter
public class TransferCommand {

    private final UUID fromAccountId;
    private final UUID toAccountId;
    private final UUID transactionId;

}
